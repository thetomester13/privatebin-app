FROM cloudron/base:1.0.0@sha256:147a648a068a2e746644746bbfb42eb7a50d682437cead3c67c933c546357617

ENV RELEASE 1.3.1

RUN mkdir -p /app/code /app/pkg /app/data
WORKDIR /app/code

COPY start.sh /app/pkg/

RUN \
# Install PrivateBin
    cd /tmp \
    && curl -Ls https://github.com/PrivateBin/PrivateBin/releases/download/${RELEASE}/PrivateBin-${RELEASE}.tar.gz.asc > PrivateBin-${RELEASE}.tar.gz.asc \
    && curl -Ls https://github.com/PrivateBin/PrivateBin/archive/${RELEASE}.tar.gz > PrivateBin-${RELEASE}.tar.gz \
    && cd /app/code \
    && tar -xzf /tmp/PrivateBin-${RELEASE}.tar.gz --strip 1 \
    && mv .htaccess.disabled .htaccess \
    && rm *.md \
    && mv cfg /app/data \
    && mv lib /app/data \
    && mv tpl /app/data \
    && mv vendor /app/data \
    && sed -i "s#define('PATH', '');#define('PATH', '/app/data/');#" index.php \
    && chown -R nobody.www-data /app/code \
    && rm -rf /tmp/*

# configure apache
RUN rm /etc/apache2/sites-enabled/*
RUN sed -e 's,^ErrorLog.*,ErrorLog "|/bin/cat",' -i /etc/apache2/apache2.conf
COPY apache/mpm_prefork.conf /etc/apache2/mods-available/mpm_prefork.conf

RUN a2disconf other-vhosts-access-log
RUN a2enmod rewrite
COPY apache/privatebin.conf /etc/apache2/sites-enabled/privatebin.conf
RUN echo "Listen 8000" > /etc/apache2/ports.conf
RUN echo "ServerName localhost" >> /etc/apache2/apache2.conf

# configure mod_php
RUN a2enmod php7.2
RUN crudini --set /etc/php/7.2/apache2/php.ini PHP upload_max_filesize 256M && \
    crudini --set /etc/php/7.2/apache2/php.ini PHP upload_max_size 256M && \
    crudini --set /etc/php/7.2/apache2/php.ini PHP post_max_size 256M && \
    crudini --set /etc/php/7.2/apache2/php.ini PHP memory_limit 256M && \
    crudini --set /etc/php/7.2/apache2/php.ini PHP max_execution_time 200 && \
    crudini --set /etc/php/7.2/apache2/php.ini Session session.gc_probability 1 && \
    crudini --set /etc/php/7.2/apache2/php.ini Session session.gc_divisor 100

RUN chown -R www-data.www-data /app/code
RUN chmod +x /app/pkg/start.sh

EXPOSE 8000

CMD [ "/app/pkg/start.sh" ]
