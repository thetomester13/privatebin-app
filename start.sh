#!/bin/bash

set -eu

# setup waits for apache to start and configures privatebin
setup() {
    while [[ ! -f "/var/run/apache2/apache2.pid" ]]; do
        echo "Waiting for apache2 to start"
        sleep 1
    done

    echo "Creating config"
    if [[ ! -f /app/data/cfg/conf.php ]]; then
        cp /app/data/cfg/conf.sample.php /app/data/cfg/conf.php

        # Enable file uploading
        sed -i "s#fileupload = false#fileupload = true#" /app/data/cfg/conf.php
    fi

    echo "Changing permissions"
    chown -R www-data:www-data /app/data/

    echo "Setup done"
}

( setup ) &

APACHE_CONFDIR="" source /etc/apache2/envvars
rm -f "${APACHE_PID_FILE}"
exec /usr/sbin/apache2 -DFOREGROUND
