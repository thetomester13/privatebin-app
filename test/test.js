#!/usr/bin/env node

/* jslint node:true */
/* global it:false */
/* global xit:false */
/* global describe:false */
/* global before:false */
/* global after:false */

'use strict';

require('chromedriver');

var execSync = require('child_process').execSync,
    expect = require('expect.js'),
    path = require('path'),
    superagent = require('superagent'),
    webdriver = require('selenium-webdriver');

var by = webdriver.By,
    logging = webdriver.logging,
    Builder = require('selenium-webdriver').Builder;

process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';

describe('Application life cycle test', function () {
    this.timeout(650000);

    var server, browser = new Builder().forBrowser('chrome').build();
    var LOCATION = 'test';
    var app;

    before(function (done) {
        var seleniumJar= require('selenium-server-standalone-jar');
        var SeleniumServer = require('selenium-webdriver/remote').SeleniumServer;
        server = new SeleniumServer(seleniumJar.path, { port: 4444 });
        server.start();

        done();
    });

    after(function (done) {
        browser.quit();
        server.kill();
        done();
    });

    it('build app', function () {
        execSync('cloudron build', { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    it('install app', function () {
        execSync('cloudron install --new --wait --location ' + LOCATION, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    it('can get app information', function () {
        var inspect = JSON.parse(execSync('cloudron inspect'));

        app = inspect.apps.filter(function (a) { return a.location === LOCATION; })[0];

        expect(app).to.be.an('object');
    });

    it('can get the main page', function (done) {
        superagent.get('https://' + app.fqdn).end(function (error, result) {
            expect(error).to.be(null);
            expect(result.status).to.eql(200);

            done();
        });
    });

    it('can set and view a "burn after reading" message only once', function(done) {
        const message = "Burn After Reading secret message";

        browser.get('https://' + app.fqdn).then(function() {
            return browser.findElement(by.xpath('//input[@id="burnafterreading"]')).click();
        }).then(function() {
            return browser.findElement(by.id('message')).sendKeys(message);
        }).then(function() {
            return browser.findElement(by.id('sendbutton')).click();
        }).then(function() {
            return browser.sleep(5000);
        }).then(function() {
            return browser.findElement(by.id('pasteurl')).getText();
        }).then(function(pasteUrl) {
            // pasteUrl is our unique secret URL
            return ;
        }).then(function() {
            return browser.navigate().refresh();
        }).then(function() {
            return browser.sleep(3000);
        }).then(function() {
            return browser.findElement(by.id('prettyprint')).getText();
        }).then(function(text) {
            expect(text).to.equal(message);
        }).then(function() {
            return browser.navigate().refresh();
        }).then(function() {
            return browser.sleep(3000);
        }).then(function() {
            return browser.findElement(by.id('prettyprint')).getText();
        }).then(function(text) {
            expect(text).to.equal('');
        }).then(function() {
            return browser.findElement(by.id('errormessage')).getText();
        }).then(function(text) {
            expect(text).to.not.be.empty();
            expect(text).to.contain('expired');

            done();
        });
    });

    it('can upload and download and read a simple text file', function(done) {
        const message = "Upload/Download Text File secret message";
        const testFilePath = `${path.resolve(__dirname, '.')}/test.txt`;

        browser.get('https://' + app.fqdn).then(function() {
            return browser.findElement(by.id('message')).sendKeys(message);
        }).then(function() {
            return browser.findElement(by.id('file')).sendKeys(testFilePath);
        }).then(function() {
            return browser.findElement(by.id('attach')).click();
        }).then(function() {
            return browser.findElement(by.id('sendbutton')).click();
        }).then(function() {
            return browser.sleep(5000);
        }).then(function() {
            return browser.findElement(by.id('pasteurl')).getText();
        }).then(function(pasteUrl) {
            // pasteUrl is our unique secret URL
            return ;
        }).then(function() {
            return browser.navigate().refresh();
        }).then(function() {
            return browser.sleep(5000);
        }).then(function() {
            return browser.findElement(by.id('prettyprint')).getText();
        }).then(function(text) {
            expect(text).to.equal(message);
        }).then(function() {
            return browser.findElement(by.id('attachment')).getText();
        }).then(function(text) {
            expect(text).to.not.be.empty();
            expect(text).to.contain('Download attachment');
        }).then(function() {
            return browser.findElement(by.xpath('//div[@id="attachment"]/a')).getAttribute('href');
        }).then(function(href) {
            expect(href).to.contain(app.fqdn);

            return browser.get(href); // Let's view our text file in the browser
        }).then(function() {
            return browser.sleep(5000);
        }).then(function() {
            return browser.findElement(by.tagName('body')).getText();
        }).then(function(bodyContents) {
            expect(bodyContents).to.be.equal("Test txt file upload");
            done();
        });
    });

    it('can set a message and delete it before it has been accessed', function(done) {
        const message = "Immediate Delete secret message";
        let binUrl;

        browser.get('https://' + app.fqdn).then(function() {
            return browser.findElement(by.id('message')).sendKeys(message);
        }).then(function() {
            return browser.findElement(by.id('sendbutton')).click();
        }).then(function() {
            return browser.sleep(5000);
        }).then(function() {
            return browser.findElement(by.id('pasteurl')).getText();
        }).then(function(pasteUrl) {
            // pasteUrl is our unique secret URL
            binUrl = pasteUrl
            return ;
        }).then(function() {
            // Click on 'Delete Data' button
            return browser.findElement(by.id('deletelink')).click();
        }).then(function() {
            return browser.sleep(3000);
        }).then(function() {
            return browser.get(binUrl);
        }).then(function() {
            return browser.sleep(3000);
        }).then(function() {
            return browser.findElement(by.id('prettyprint')).getText();
        }).then(function(text) {
            expect(text).to.equal('');
        }).then(function() {
            return browser.findElement(by.id('errormessage')).getText();
        }).then(function(text) {
            expect(text).to.not.be.empty();
            expect(text).to.contain('deleted');

            done();
        });
    });

    it('can set and view a message only with the proper password', function(done) {
      const message = "Password protected secret message";
      const password = "some$ecr3tPassw0rd!";

      browser.get('https://' + app.fqdn).then(function() {
          return browser.findElement(by.id('passwordinput')).sendKeys(password);
      }).then(function() {
          return browser.findElement(by.id('message')).sendKeys(message);
      }).then(function() {
          return browser.findElement(by.id('sendbutton')).click();
      }).then(function() {
          return browser.sleep(5000);
      }).then(function() {
          return browser.findElement(by.id('pasteurl')).getText();
      }).then(function(pasteUrl) {
          // pasteUrl is our unique secret URL
          return ;
      }).then(function() {
          return browser.navigate().refresh();
      }).then(function() {
          return browser.sleep(3000);
      }).then(function() {
          return browser.findElement(by.id('passworddecrypt')).sendKeys('wrongPassword');
      }).then(function() {
        return browser.findElement(by.xpath('//form[@id="passwordform"]/button[@type="submit"]')).click(); // Try to unlock bin with wrong password
      }).then(function() {
          return browser.sleep(3000);
      }).then(function() {
          return browser.findElement(by.id('errormessage')).getText();
      }).then(function(text) {
          const lowerCaseText = text.toLowerCase();
          expect(lowerCaseText).to.contain("wrong password");
          expect(lowerCaseText).to.contain("retry");

          return browser.sleep(3000);
      }).then(function() {
          return browser.findElement(by.id('retrybutton')).click();
      }).then(function() {
          return browser.sleep(3000);
      }).then(function() {
          return browser.findElement(by.id('passworddecrypt')).sendKeys(password);
      }).then(function() {
        return browser.findElement(by.xpath('//form[@id="passwordform"]/button[@type="submit"]')).click(); // Try to unlock bin with correct password
      }).then(function() {
        return browser.sleep(3000);
      }).then(function() {
          return browser.findElement(by.id('prettyprint')).getText();
      }).then(function(text) {
          expect(text).to.equal(message);
          done();
      });
  });

    it('can set and view a 5 minute timed message only for 5 minutes', function(done) {
        const message = "Five minute timed secret message";
        this.timeout(950000);

        browser.get('https://' + app.fqdn).then(function() {
            return browser.findElement(by.id('expiration')).click();
        }).then(function() {
            return browser.findElement(by.xpath('//a[@data-expiration="5min"]')).click();
        }).then(function() {
            return browser.findElement(by.id('message')).sendKeys(message);
        }).then(function() {
            return browser.findElement(by.id('sendbutton')).click();
        }).then(function() {
            return browser.sleep(5000);
        }).then(function() {
            return browser.findElement(by.id('pasteurl')).getText();
        }).then(function(pasteUrl) {
            // pasteUrl is our unique secret URL
            return ;
        }).then(function() {
            return browser.navigate().refresh();
        }).then(function() {
            return browser.sleep(3000);
        }).then(function() {
            return browser.findElement(by.id('prettyprint')).getText();
        }).then(function(text) {
            expect(text).to.equal(message);
        }).then(function() {
            return browser.sleep(1000 * 60 * 3); // Wait 3 minutes
        }).then(function() {
            return browser.findElement(by.id('prettyprint')).getText();
        }).then(function(text) {
            expect(text).to.equal(message);
        }).then(function() {
            return browser.sleep(1000 * 60 * 3); // Wait another 3 minutes
        }).then(function() {
            return browser.navigate().refresh();
        }).then(function() {
            return browser.sleep(3000);
        }).then(function() {
            return browser.findElement(by.id('prettyprint')).getText();
        }).then(function(text) {
            expect(text).to.equal('');
        }).then(function() {
            return browser.findElement(by.id('errormessage')).getText();
        }).then(function(text) {
            expect(text).to.not.be.empty();
            expect(text).to.contain('expired');

            done();
        });
    });

    it('can create a burn after reading, password protected, file upload bin that will be present after a backup and restore', function(done) {
        const message = "Password protected, burn after reading, uploaded file secret message, with a backup and restore";
        const password = "some$ecr3tPassw0rd!";
        const testFilePath = `${path.resolve(__dirname, '.')}/test.txt`;
        var binUrl;

        browser.get('https://' + app.fqdn).then(function() {
            return browser.findElement(by.xpath('//input[@id="burnafterreading"]')).click();
        }).then(function() {
          return browser.findElement(by.id('passwordinput')).sendKeys(password);
        }).then(function() {
            return browser.findElement(by.id('message')).sendKeys(message);
        }).then(function() {
            return browser.findElement(by.id('file')).sendKeys(testFilePath);
        }).then(function() {
            return browser.findElement(by.id('attach')).click();
        }).then(function() {
            return browser.findElement(by.id('sendbutton')).click();
        }).then(function() {
            return browser.sleep(5000);
        }).then(function() {
            return browser.findElement(by.id('pasteurl')).getText();
        }).then(function(pasteUrl) {
            // pasteUrl is our unique secret URL
            binUrl = pasteUrl;
            return ;
        }).then(function() {
            // Backup our app
            execSync('cloudron backup create --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
        }).then(function() {
            return browser.sleep(5000);
        }).then(function() {
            // Restore our app
            execSync('cloudron restore --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
        }).then(function() {
            return browser.sleep(5000);
        }).then(function() {
            return browser.navigate().refresh();
        }).then(function() {
            return browser.sleep(3000);
        }).then(function() {
            return browser.findElement(by.id('passworddecrypt')).sendKeys(password);
        }).then(function() {
          return browser.findElement(by.xpath('//form[@id="passwordform"]/button[@type="submit"]')).click(); // Try to unlock bin with correct password
        }).then(function() {
          return browser.sleep(3000);
        }).then(function() {
            return browser.findElement(by.id('prettyprint')).getText();
        }).then(function(text) {
            expect(text).to.equal(message);
        }).then(function() {
            return browser.findElement(by.id('attachment')).getText();
        }).then(function(text) {
            expect(text).to.not.be.empty();
            expect(text).to.contain('Download attachment');
        }).then(function() {
            return browser.findElement(by.xpath('//div[@id="attachment"]/a')).getAttribute('href');
        }).then(function(href) {
            expect(href).to.contain(app.fqdn);

            return browser.get(href); // Let's view our text file in the browser
        }).then(function() {
            return browser.sleep(5000);
        }).then(function() {
            return browser.findElement(by.tagName('body')).getText();
        }).then(function(bodyContents) {
            expect(bodyContents).to.be.equal("Test txt file upload");
        }).then(function() {
            return browser.get(binUrl);
        }).then(function() {
          return browser.sleep(3000);
        }).then(function() {
            return browser.findElement(by.id('prettyprint')).getText();
        }).then(function(text) {
            expect(text).to.equal('');
        }).then(function() {
            return browser.findElement(by.id('errormessage')).getText();
        }).then(function(text) {
            expect(text).to.not.be.empty();
            expect(text).to.contain('expired');

            done();
        });
    });

    it('can restart app', function (done) {
        execSync('cloudron restart --wait --app ' + app.id);
        done();
    });

    it('backup app', function () {
        execSync('cloudron backup create --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    it('restore app', function () {
        execSync('cloudron restore --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    it('can update', function () {
        execSync('cloudron install --wait --app ' + LOCATION, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    it('uninstall app', function () {
        execSync('cloudron uninstall --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });
});
